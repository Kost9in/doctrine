# Tool for DB migration

---

### Commands

 - сheck connection to DB:
    ```
    php migrate test
    ```

 - get config:
    ```
    php migrate config
    ```

 - set config:
    ```
    php migrate config mysql://user_name:password@host/DB_name
    ```

 - create new schema:
    ```
    php migrate create schema_title comment?
    ```
