<?php

class Migration {

  private $arguments;
  private $changes;
  private $config;
  private $app_config;
  private $connection;

  public function __construct($arguments, $changes, $config) {

    if (!file_exists($config['config_path'])) {
      Migration::showError('Config not found!');
    }

    $this->arguments = $arguments;
    $this->changes = $changes;
    $this->config = $config;
    $this->app_config = json_decode(file_get_contents($this->config['config_path']), 1);

    if (count($this->arguments) < 2) {
      Migration::showError('Command not found!');
    }

  }

  public function run() {
    switch ($this->arguments[1]) {

      case 'test': // сheck connection to DB
        try {
          $result = $this->ping($this->app_config);
        } catch (Exception $e) {
          $result = false;
        }
        if ($result) {
          Migration::showSuccess('Connection successful!');
        } else {
          Migration::showError('Connection failed!');
        }
      break;

      case 'config': // get/set config
        if (count($this->arguments) < 3) {
          Migration::showSuccess($this->app_config['url']);
        }
        $new_config = array('url' => $this->arguments[2]);
        try {
          $result = $this->ping($new_config);
        } catch (Exception $e) {
          $result = false;
        }
        if ($result) {
          $this->setConfig('url', $this->arguments[2]);
          Migration::showSuccess('Connection successful!'.PHP_EOL.'Config updated!');
        } else {
          Migration::showError('Connection failed!'.PHP_EOL.'Config not updated!');
        }
      break;

      case 'log': // show logs
        $log = '';
        if (count($this->arguments) < 3) {
          $changes = $this->changes->get();
          if ($changes) {
            $log = ' ID      | Time                | Comment';
            for ($i = count($changes) - 1; $i >= 0; $i--) {
              $log .= PHP_EOL.' #'.$changes[$i]['id'].' | ';
              $log .= date('d/m/Y H:i:s', $changes[$i]['time']).' | ';
              $log .= $changes[$i]['comment'];
            }
          } else {
            $log = 'Commits not found.';
          }
        } else {
          $change = $this->changes->get($this->arguments[2]);
          if ($change) {
            $log = 'ID: #'.$change['id'].PHP_EOL;
            $log .= 'Time: '.date('d/m/Y H:i:s', $change['time']).PHP_EOL;
            $log .= 'Comment: '.$change['comment'].PHP_EOL;
            $log .= 'SQL: '.$change['sql'];
          } else {
            $log = 'Commit not found.';
          }
        }
        Migration::showSuccess($log);
      break;

      //
      // case 'create': // create new schema
      //   if (count($this->arguments) < 3) {
      //     $this->showError('Schema title not found!');
      //   }
      //   $schema_title = $this->arguments[2];
      //   $filepath = $this->config['schemas_folder'].$schema_title.'.json';
      //   if (file_exists($filepath)) {
      //     $this->showError('Schema "'.$schema_title.'" already exists!');
      //   }
      //   $this->connect($this->config_DB);
      //   $sm = $this->connection->getSchemaManager();
      //   $schema = array(
      //     'title' => $schema_title,
      //     'time' => time(),
      //     'comment' => (count($this->arguments) < 4) ? '' : $this->arguments[3],
      //     'schema' => serialize($sm->createSchema())
      //   );
      //   file_put_contents($filepath, json_encode($schema));
      //   $this->setConfig('schema', $schema_title);
      //   $this->showSuccess('Schema "'.$schema_title.'" created successful!');
      // break;
      //
      // case 'change': // change schema
      //   if (count($this->arguments) < 3) {
      //     $this->showError('Schema title not found!');
      //   }
      //   $schema_title = $this->arguments[2];
      //   $filepath = $this->config['schemas_folder'].$schema_title.'.json';
      //   if (!file_exists($filepath)) {
      //     $this->showError('Schema "'.$schema_title.'" not found!');
      //   }
      //   $this->setConfig('schema', $schema_title);
      //   $this->showSuccess('Active schema changed successful!');
      // break;
      //
      case 'commit': // add new change
        if (count($this->arguments) < 3) {
          $this->showError('Comment not found!');
        }
        $this->connect($this->app_config);
        $schema = $this->loadSchema();
        // $this->connect($this->app_config);
        // $sm = $this->connection->getSchemaManager();
        // $new_schema = $sm->createSchema();
        // $diff = $this->getDiff($schema, $new_schema);
        // if ($diff) {
        //   $diff_string = implode(';'.PHP_EOL, $diff);
        //   $this->changes->add(array('comment'=>$this->arguments[2],'sql'=>$diff_string));
        // } else {
        //   Migration::showSuccess('Already up-to-date.');
        // }


        // if (count($this->arguments) < 3) {
        //   $this->showError('Comment not found!');
        // }
        // $comment = $this->arguments[2];
        // $old_schema = $this->loadLastSchema();
        // $this->connect($this->config_DB);
        // $sm = $this->connection->getSchemaManager();
        // $new_schema = $sm->createSchema();
        // $diff = $this->getDiff($old_schema, $new_schema);
        // if (count($diff) === 0) {
        //   $this->showSuccess('Nothing to commit.');
        // }
        //
        // print_r($diff);
        // $this->showSuccess('commit! '.$comment);
      break;

      default:
        Migration::showError('Command not found!');
      break;

    }
  }

  private function connect($config) {
    if (!$this->connection) {
      $this->connection = \Doctrine\DBAL\DriverManager::getConnection($config, new \Doctrine\DBAL\Configuration());
    }
  }

  private function ping($config) {
    $this->connect($config);
    return $this->connection->ping();
  }

  private function setConfig($key, $value) {
    $this->app_config[$key] = $value;
    return file_put_contents($this->config['config_path'], json_encode($this->app_config));
  }

  private function loadSchema() {

    $schema = new \Doctrine\DBAL\Schema\Schema();

    print_r($schema->toSql($this->connection->getDatabasePlatform()));

    $changes = $this->changes->get();
    if ($changes) {
      foreach ($changes as $v) {
        print_r($v);
        // $schema->setSql($v);
      }
    }

    print_r($schema->toSql($this->connection->getDatabasePlatform()));


    return $schema;

  }
  //
  // private function loadLastSchema() {
  //   return $this->loadSchema();
  // }

  private function getDiff($old_schema, $new_schema) {
    return $old_schema->getMigrateToSql($new_schema, $this->connection->getDatabasePlatform());
    // $comparator = new \Doctrine\DBAL\Schema\Comparator();
    // $schemaDiff = $comparator->compare($old_schema, $new_schema);
    // return $schemaDiff->toSql($this->connection->getDatabasePlatform());
  }

  static public function showError($error) {
    self::showResult($error, 31);
  }

  static public function showSuccess($success) {
    self::showResult($success, 32);
  }

  private function showResult($result, $color) {
    exit(PHP_EOL."\033[".$color."m".$result."\033[0m".PHP_EOL.PHP_EOL);
  }

}
