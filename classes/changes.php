<?php

class Changes {

  private $config;
  private $changes;

  public function __construct ($config) {
    if (!file_exists($config['path'])) {
      mkdir($config['path'], 0755);
    }
    $this->config = $config;
    $this->changes = $this->loadChanges();
  }

  public function get ($id = '') {
    if ($id) {
      $change = [];
      foreach ($this->changes as $v) {
        if ($v['id'] === $id) {
          $change = $v;
        }
      }
      return $change;
    }
    return $this->changes;
  }

  public function add ($change) {
    $result = array('id'=>'','error'=>'');
    if (isset($change['comment'], $change['sql']) && count($change) === 2) {
      $change['time'] = time();
      $change['id'] = $this->newId($change['time']);
      $filepath = $this->config['path'].$change['id'].'.json';
      if (file_put_contents($filepath, json_encode($change))) {
        $result['id'] = $change['id'];
        $this->changes[] = $change;
      } else {
        $result['error'] = 'Change not saved!';
      }
    } else {
      $result['error'] = 'Error change!';
    }
    return $result;
  }

  public function remove ($id) {
    $result = false;
    if ($this->get($id)) {
      $path = $this->config['path'].$id.'.json';
      if (file_exists($path)) {
        $result = unlink($path);
      }
    }
    return $result;
  }

  private function loadChanges () {
    $changes = [];
    $files = scandir($this->config['path']);
    foreach ($files as $v) {
      $path = $this->config['path'].$v;
      $content = json_decode(file_get_contents($path), 1);
      if (file_exists($path) && isset($content['id'], $content['sql'], $content['time'], $content['comment'])) {
        $changes[] = $content;
      }
    }
    function sortCallback ($a, $b) {
      return ($a['time'] < $b['time']) ? -1 : 1;
    }
    usort($changes, 'sortCallback');
    return $changes;
  }

  private function newId ($time) {
    $nubmer = 0;
    do {
      $nubmer++;
      $new_hash = md5($time.$nubmer);
      $new_id = substr($new_hash, 0, 6);
    } while ($this->get($new_id));
    return $new_id;
  }

}
